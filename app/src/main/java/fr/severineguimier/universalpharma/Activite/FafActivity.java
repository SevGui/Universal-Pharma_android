package fr.severineguimier.universalpharma.Activite;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import fr.severineguimier.universalpharma.Dao.Dao;
import fr.severineguimier.universalpharma.Dao.Param;
import fr.severineguimier.universalpharma.Metier.Faf;
import fr.severineguimier.universalpharma.R;

/**
 * Created by Severine on 01/07/2016.
 */
public class FafActivity extends AppCompatActivity {
    private Spinner spinner;
    private String categorieTF;
    private Dao dao;
    private Faf faf1;
    private int index;

    private Cursor c;
    private SimpleCursorAdapter adapter;
    private ListView listView;
    private TextView etId;
    private EditText etQuantite;
    private Button bt_Add;
    private Button bt_Update;
    private Button bt_Delete;

    private Button bt_ValidPop;
    private Button bt_AnnulPop;

    private String action;
    private String msgpop;
    private PopupWindow pwindo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faf);

        //permet de remplir la liste de choix
        spinner = (Spinner) findViewById(R.id.categories_spinner);
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.categories_spinner, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        dao = new Dao(this);

        peuplerListeView();

        listView = (ListView) findViewById(R.id.lvlist_faf);
        etId =(TextView) findViewById(R.id.idFaf);
        etQuantite =(EditText) findViewById(R.id.editQuantite);
        bt_Add = (Button) findViewById(R.id.btAdd_Faf);
        bt_Update = (Button) findViewById(R.id.btUpdate_Faf);
        // Permet de mettre le bouton invisible
        bt_Update.setVisibility(View.GONE);
        bt_Delete = (Button) findViewById(R.id.btDelete_Faf);
        bt_Delete.setVisibility(View.GONE);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String faf_ID, categorie, quantite;

                Cursor ligne = (Cursor) parent.getItemAtPosition(position);

                faf_ID = ligne.getString(0);
                categorie = ligne.getString(1);
                quantite = ligne.getString(2);

                etId.setText(faf_ID);
                if (categorie.equals("ETP")) index = 0;
                if (categorie.equals("NUI")) index = 1;
                if (categorie.equals("REP")) index = 2;
                if (categorie.equals("4cvd")) index = 3;
                if (categorie.equals("4cve")) index = 4;
                if (categorie.equals("56cvd")) index = 5;
                if (categorie.equals("56cve")) index = 6;
                spinner.setSelection(index);
                etQuantite.setText(quantite);
                bt_Update.setVisibility(View.VISIBLE);
                bt_Delete.setVisibility(View.VISIBLE);
                bt_Add.setVisibility(View.GONE);

            }
        });

    }
//--------------------------------------------------------------------------------------------------
    //permet de remplir la liste
    public void peuplerListeView(){
        c = dao.selectFaf();
        adapter = new SimpleCursorAdapter(this, R.layout.items_faf,
                c, new String[]{Param.COL_ID_FAF, Param.COL_TF_FAF,Param.COL_QUANTITE_FAF},
                new int[]{R.id.tvId_faf, R.id.tvCategorie_faf, R.id.tvQuantite_faf});

        ListView listView = (ListView) findViewById(R.id.lvlist_faf);
        listView.setAdapter(adapter);
    }

    public void ajouterFaf(View v){
        categorieTF = "";

        spinner = (Spinner) findViewById(R.id.categories_spinner);
        if (spinner.getSelectedItem().equals("Etapes")) categorieTF = "ETP";
        if (spinner.getSelectedItem().equals("Hotel")) categorieTF = "NUI";
        if (spinner.getSelectedItem().equals("Repas")) categorieTF = "REP";
        if (spinner.getSelectedItem().equals("4ch diesel")) categorieTF = "4cvd";
        if (spinner.getSelectedItem().equals("4ch essence")) categorieTF = "4cve";
        if (spinner.getSelectedItem().equals("5/6ch diesel")) categorieTF = "56cvd";
        if (spinner.getSelectedItem().equals("5/6ch essence")) categorieTF = "56cve";

        etQuantite =(EditText) findViewById(R.id.editQuantite);

        //instanciation avec , la sélection du spinner et la quantité ramenée en entier
        faf1 = new Faf(categorieTF, Integer.parseInt(etQuantite.getText().toString()));
/*        Toast toast = Toast.makeText(FafActivity.this, date +", "+spinner.getSelectedItem().toString()+", "+Integer.parseInt(etQuantite.getText().toString()), Toast.LENGTH_LONG);
        toast.show();*/
        Long resu = dao.createFaf(faf1);
        Toast toast = Toast.makeText(FafActivity.this, "Création de la ligne n°" + resu, Toast.LENGTH_LONG);
        toast.show();
        peuplerListeView();
        initialiser();
    }

    public void modifierFaf(View view) {
/*        Toast ntoast = Toast.makeText(FafActivity.this, "Bouton Modifier", Toast.LENGTH_LONG);
        ntoast.show();*/
        action = "Update";
        msgpop = "Voulez vous modifier ?";
        initiatePopupWindow();
    }


    public void supprimerFaf(View view) {
/*        Toast ntoast = Toast.makeText(FafActivity.this, "Bouton Supprimer", Toast.LENGTH_LONG);
        ntoast.show();*/
        action = "Delete";
        msgpop = "Voulez vous supprimer ?";
        initiatePopupWindow();
    }

    //permet de réinitialiser les champs
    private void initialiser() {
        spinner.setSelection(0);
        etId.setText("");
        etQuantite.setText("");
        bt_Update.setVisibility(View.GONE);
        bt_Delete.setVisibility(View.GONE);
        bt_Add.setVisibility(View.VISIBLE);
    }

    private void initiatePopupWindow() {
        categorieTF = "";
        try {
            // Instanciation de LayoutInflater
            LayoutInflater inflater = (LayoutInflater) FafActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.popupwindow,
                    (ViewGroup) findViewById(R.id.popup_1));
            TextView tvpop = (TextView) layout.findViewById(R.id.tvPop);
            tvpop.setText(msgpop);

            pwindo = new PopupWindow(layout, 350, 180, true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

            bt_ValidPop = (Button) layout.findViewById(R.id.valider_popup);
            bt_ValidPop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etId =(TextView) findViewById(R.id.idFaf);

                    spinner = (Spinner) findViewById(R.id.categories_spinner);
                    if (spinner.getSelectedItem().equals("Etapes")) categorieTF = "ETP";
                    if (spinner.getSelectedItem().equals("Hotel")) categorieTF = "NUI";
                    if (spinner.getSelectedItem().equals("Repas")) categorieTF = "REP";
                    if (spinner.getSelectedItem().equals("4ch diesel")) categorieTF = "4cvd";
                    if (spinner.getSelectedItem().equals("4ch essence")) categorieTF = "4cve";
                    if (spinner.getSelectedItem().equals("5/6ch diesel")) categorieTF = "56cvd";
                    if (spinner.getSelectedItem().equals("5/6ch essence")) categorieTF = "56cve";
                    etQuantite =(EditText) findViewById(R.id.editQuantite);

                    faf1 = new Faf(categorieTF, Integer.parseInt(etQuantite.getText().toString()));
                    faf1.setId(Long.parseLong(etId.getText().toString()));
                    String msg = "";
                    switch (action){
                        case "Delete":
                            dao.deleteFaf(faf1);
                            msg = "Suppression du frais";
                            break;
                        case "Update":
                            dao.updateFaf(faf1);
                            msg = "Modification du frais";
                            break;

                    }
                    Toast toast = Toast.makeText(FafActivity.this, msg, Toast.LENGTH_LONG);
                    toast.show();
                    peuplerListeView();
                    initialiser();

                    pwindo.dismiss();
                }
            });

            bt_AnnulPop = (Button) layout.findViewById(R.id.annuler_popup);
            bt_AnnulPop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast toast = Toast.makeText(FafActivity.this, "Annulation", Toast.LENGTH_LONG);
                    toast.show();
                    peuplerListeView();
                    initialiser();

                    pwindo.dismiss();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void retourFaf(View view) {
        //initialise un transfert d'activity
        Intent myIntent = new Intent(FafActivity.this, TabbordActivity.class);
        //lance le transfert
        startActivity(myIntent);
    }
}
