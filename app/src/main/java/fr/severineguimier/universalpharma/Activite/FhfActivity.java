package fr.severineguimier.universalpharma.Activite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.severineguimier.universalpharma.Dao.Dao;
import fr.severineguimier.universalpharma.Dao.Param;
import fr.severineguimier.universalpharma.Metier.Fhf;
import fr.severineguimier.universalpharma.R;

public class FhfActivity extends AppCompatActivity {
    private Dao dao;
    private ListView listView;
    private TextView etId;
    private EditText etLibelle;
    private EditText etMontant;
    private EditText etDateJ;
    private TextView etNomJ;
    private Fhf fhf1;

    private Button bt_Add;
    private Button bt_Update;
    private Button bt_Delete;

    private View layDateJ;
    private View layNomJ;
    private View layPhoto;

    private Cursor c;
    private SimpleCursorAdapter adapter;

    private Button bt_ValidPop;
    private Button bt_AnnulPop;

    private String action;
    private String msgpop;
    private PopupWindow pwindo;

    private File photo;
    private String filename;
    private String dateJour;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fhf);

        dao = new Dao(this);

        peuplerListeView();

        listView = (ListView) findViewById(R.id.lvlist_fhf);
        etId = (TextView) findViewById(R.id.idFhf);
        etId.setVisibility(View.GONE);
        etLibelle = (EditText) findViewById(R.id.editLibelle);
        etMontant = (EditText) findViewById(R.id.editMontant);
        etDateJ = (EditText) findViewById(R.id.editDateJ);
        etNomJ = (TextView) findViewById(R.id.editNomJ);
        bt_Add = (Button) findViewById(R.id.btAdd_Fhf);
        bt_Update = (Button) findViewById(R.id.btUpdate_Fhf);
        // Permet de mettre le bouton invisible
        bt_Update.setVisibility(View.GONE);
        bt_Delete = (Button) findViewById(R.id.btDelete_Fhf);
        bt_Delete.setVisibility(View.GONE);

        layDateJ = (View) findViewById(R.id.linearDateJ);
        layDateJ.setVisibility(View.GONE);
        layNomJ = (View) findViewById(R.id.linearNomJ);
        layNomJ.setVisibility(View.GONE);
        layPhoto = (View) findViewById(R.id.linearPhoto);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String fhf_ID, libelle, montant, dateJ, nomJ;

                Cursor ligne = (Cursor) parent.getItemAtPosition(position);

                fhf_ID = ligne.getString(0);
                libelle = ligne.getString(1);
                montant = ligne.getString(2);
                dateJ = ligne.getString(3);
                nomJ = ligne.getString(4);

                etId.setVisibility(View.VISIBLE);
                etId.setText(fhf_ID);
                etLibelle.setText(libelle);
                etMontant.setText(montant);
                etDateJ.setText(dateJ);
                etNomJ.setText(nomJ);

                bt_Update.setVisibility(View.VISIBLE);
                bt_Delete.setVisibility(View.VISIBLE);
                bt_Add.setVisibility(View.GONE);

                layDateJ.setVisibility(View.VISIBLE);
                layNomJ.setVisibility(View.VISIBLE);
                layPhoto.setVisibility(View.GONE);

            }
        });
    }

    //--------------------------------------------------------------------------------------------------
    //permet de remplir la liste
    private void peuplerListeView() {
        c = dao.selectFhf();
        adapter = new SimpleCursorAdapter(this, R.layout.items_fhf,
                c, new String[]{Param.COL_ID_FHF, Param.COL_LIBELLE_FHF, Param.COL_MONTANT_FHF, Param.COL_DATEJ_FHF, Param.COL_NOMJUSTIF_FHF},
                new int[]{R.id.tvId_fhf, R.id.tvLibelle, R.id.tvMontant, R.id.tvDateJ, R.id.tvNomJ});

        ListView listView = (ListView) findViewById(R.id.lvlist_fhf);
        listView.setAdapter(adapter);
    }

    //--------------------------------------------------------------------------------------------------
    //permet d'ajouter un frais hors forfait à la bdd
    public void ajouterFhf(View v) {

        etLibelle = (EditText) findViewById(R.id.editLibelle);
        etMontant = (EditText) findViewById(R.id.editMontant);
        etDateJ = (EditText) findViewById(R.id.editDateJ);
        etNomJ = (TextView) findViewById(R.id.editNomJ);

        //instanciation avec le libelle, le montant, la date du justif et le nom du justif
        fhf1 = new Fhf(etLibelle.getText().toString(), Float.parseFloat(etMontant.getText().toString()), etDateJ.getText().toString(), etNomJ.getText().toString());

        Long resu = dao.createFhf(fhf1);
        Toast toast = Toast.makeText(FhfActivity.this, "Création de la ligne n°" + resu, Toast.LENGTH_LONG);
        toast.show();
        peuplerListeView();
        initialiser();
    }

    //--------------------------------------------------------------------------------------------------
    //permet de modifier un frais hors forfait à la bdd
    public void modifierFhf(View view) {
/*        Toast ntoast = Toast.makeText(FafActivity.this, "Bouton Modifier", Toast.LENGTH_LONG);
        ntoast.show();*/
        action = "Update";
        msgpop = "Voulez vous modifier ?";
        initiatePopupWindow();
    }

    //--------------------------------------------------------------------------------------------------
    //permet de supprimer un frais hors forfait à la bdd
    public void supprimerFhf(View view) {
/*        Toast ntoast = Toast.makeText(FafActivity.this, "Bouton Supprimer", Toast.LENGTH_LONG);
        ntoast.show();*/
        action = "Delete";
        msgpop = "Voulez vous supprimer ?";
        initiatePopupWindow();
    }

    //--------------------------------------------------------------------------------------------------
    //permet de réinitialiser les champs
    private void initialiser() {
        etId.setText("");
        etId.setVisibility(View.GONE);
        etLibelle.setText("");
        etMontant.setText("");
        etDateJ.setText("");
        layDateJ.setVisibility(View.GONE);
        etNomJ.setText("");
        layNomJ.setVisibility(View.GONE);
        layPhoto.setVisibility(View.VISIBLE);

        bt_Update.setVisibility(View.GONE);
        bt_Delete.setVisibility(View.GONE);
        bt_Add.setVisibility(View.VISIBLE);
    }

    //--------------------------------------------------------------------------------------------------
    //permet de créer une petite fenêtre pour la confirmation de la modification ou de la suppression
    private void initiatePopupWindow() {

        try {
            // Instanciation de LayoutInflater
            LayoutInflater inflater = (LayoutInflater) FhfActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.popupwindow,
                    (ViewGroup) findViewById(R.id.popup_1));
            TextView tvpop = (TextView) layout.findViewById(R.id.tvPop);
            tvpop.setText(msgpop);

            pwindo = new PopupWindow(layout, 350, 180, true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

            bt_ValidPop = (Button) layout.findViewById(R.id.valider_popup);
            bt_ValidPop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etId = (TextView) findViewById(R.id.idFhf);
                    etLibelle = (EditText) findViewById(R.id.editLibelle);
                    etMontant = (EditText) findViewById(R.id.editMontant);
                    etDateJ = (EditText) findViewById(R.id.editDateJ);
                    etNomJ = (TextView) findViewById(R.id.editNomJ);

                    fhf1 = new Fhf(etLibelle.getText().toString(), Float.parseFloat(etMontant.getText().toString()), etDateJ.getText().toString(), etNomJ.getText().toString());
                    fhf1.setId(Long.parseLong(etId.getText().toString()));
                    String msg = "";
                    switch (action) {
                        case "Delete":
                            dao.deleteFhf(fhf1);
                            msg = "Suppression du frais";
                            break;
                        case "Update":
                            dao.updateFhf(fhf1);
                            msg = "Modification du frais";
                            break;

                    }
                    Toast toast = Toast.makeText(FhfActivity.this, msg, Toast.LENGTH_LONG);
                    toast.show();
                    peuplerListeView();
                    initialiser();

                    pwindo.dismiss();
                }
            });

            bt_AnnulPop = (Button) layout.findViewById(R.id.annuler_popup);
            bt_AnnulPop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast toast = Toast.makeText(FhfActivity.this, "Annulation", Toast.LENGTH_LONG);
                    toast.show();
                    peuplerListeView();
                    initialiser();

                    pwindo.dismiss();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //--------------------------------------------------------------------------------------------------
    //permet de retourner au tableau de bord
    public void retourFhf(View view) {
        //initialise un transfert d'activity
        Intent myIntent = new Intent(FhfActivity.this, TabbordActivity.class);
        //lance le transfert
        startActivity(myIntent);
    }

    //--------------------------------------------------------------------------------------------------
    //permet de prendre une photo, de récupérer la date et de créer un nom pour la photo
    public void prendrePhoto(View view) throws InterruptedException {

        //crée une instance de l'appareil photo
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        //met la date d'aujourd'hui au format yyyyMMdd_HHmmss
        String dateFacture = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        Log.v("Photo1", dateFacture);

        //crée le nom du fichier avec la date au format précédent
        filename = "Fact" + dateFacture + ".jpg";
        Log.v("Photo2", filename);

        //met la date d'aujourd'hui au format yyyy-MM-dd
        dateJour = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Log.v("Photo3", dateJour);

        //Endroit où la photo sera enregistré
        photo = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), filename);
        Log.v("Photo4", photo.toString());
        // On récupère ensuite l'URI associée au fichier
        Uri tempUri = Uri.fromFile(photo);

        // Et on déclare qu'on veut que l'image soit enregistrée là où pointe l'URI
        intent.putExtra(MediaStore.EXTRA_OUTPUT, tempUri);
        //définit la qualité de la photo 1: haute qualité, 0: basse qualité
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        //en argument l'intent puis le code de retour pour savoir si cela a fonctionné
        startActivityForResult(intent, 100);

    }

    //--------------------------------------------------------------------------------------------------
    //permet de choisir une photo, de récupérer la date et de créer un nom pour la photo
    public void choisirPhoto(View view) throws InterruptedException {

        //Crée une instance de la gallerie
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        //met la date d'aujourd'hui au format yyyyMMdd_HHmmss
        String dateFacture = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        Log.v("PhotoGallery1", dateFacture);

        //crée le nom du fichier avec la date au format précédent
        filename = "Fact" + dateFacture + ".jpg";
        Log.v("PhotoGallery2", filename);

        //met la date d'aujourd'hui au format yyyy-MM-dd
        dateJour = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Log.v("PhotoGallery3", dateJour);

        //Endroit où la photo sera enregistré
        photo = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),filename);
        Log.v("PhotoGallery4", photo.toString());
        // On récupère ensuite l'URI associée au fichier
        Uri tempUri = Uri.fromFile(photo);

        //set the data and type, get all image types
        photoPickerIntent.setDataAndType(tempUri,"image/*");

        startActivityForResult(photoPickerIntent, Param.IMAGE_GALLERY_REQUEST);
    }

    //Résultat de la photo et de la gallery : Insertion du nom et de la date dans les champs en fonction
    // de l'enregistrement de la photo
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    if (photo.exists()) {
//                        Toast.makeText(this, "La photo a été sauvegardée dans " + photo.getAbsolutePath(), Toast.LENGTH_LONG).show();
                        etNomJ.setText(filename);
                        etDateJ.setText(dateJour);
                        layDateJ.setVisibility(View.VISIBLE);
                        layNomJ.setVisibility(View.VISIBLE);
                        layPhoto.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(this, "Une erreur s'est produite lors de la sauvegarde", Toast.LENGTH_LONG).show();
                    }
                    break;
                case Activity.RESULT_CANCELED:
                    break;
                default:
                    break;
            }
        } else if (requestCode == Param.IMAGE_GALLERY_REQUEST) {
//            Toast.makeText(this,"Partie Request Gallery", Toast.LENGTH_LONG).show();
            switch (resultCode) {
                case Activity.RESULT_OK:
//                    Toast.makeText(this, "Partie Request Gallery et result OK", Toast.LENGTH_LONG).show();

                    //adresse de l'image sur la sdcard
                    Uri selectedImage = data.getData();
                    Log.v("PhotoGallery5", selectedImage.toString());
                    Cursor imgCursor = managedQuery(selectedImage, new String[]{
                                    MediaStore.Images.ImageColumns.DATA,
                                    MediaStore.Images.ImageColumns.ORIENTATION},
                            null, null, null);
                    imgCursor.moveToFirst();

                    //Récupère le fichier photo des données
                    File tempFile = new File(imgCursor.getString(0));
                    Log.v("PhotoGallery6", tempFile.getAbsolutePath());

                    //Endroit où la photo sera enregistré
                    photo = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), filename);
                    Log.v("PhotoGallery7", photo.toString());
                    //on renomme la photo avec modification du chemin
                    tempFile.renameTo(photo);

                    if (photo.exists()) {
//                        Toast.makeText(this, "La photo a été sauvegardée dans " + photo.getAbsolutePath(), Toast.LENGTH_LONG).show();
                        etNomJ.setText(filename);
                        etDateJ.setText(dateJour);
                        layDateJ.setVisibility(View.VISIBLE);
                        layNomJ.setVisibility(View.VISIBLE);
                        layPhoto.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(this, "Une erreur s'est produite lors de la sauvegarde", Toast.LENGTH_LONG).show();
                    }

                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(this, "Partie Request Gallery et result abandonné", Toast.LENGTH_LONG).show();
                    break;
                default:
                    break;
            }
        }
    }


}
