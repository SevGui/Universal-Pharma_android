package fr.severineguimier.universalpharma.Activite;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import fr.severineguimier.universalpharma.Dao.Dao;
import fr.severineguimier.universalpharma.Dao.Param;
import fr.severineguimier.universalpharma.Metier.User;
import fr.severineguimier.universalpharma.R;

public class MainActivity extends AppCompatActivity {

    //1° declarer une variable de type widget : Button
    private Button bt_valider;
    private EditText et_password;
    private Dao dao;
    private Cursor c;
    private User user1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dao = new Dao(this);
        c = dao.selectOneUser(Param.MATRICULE_USER);
        c.moveToFirst();
        user1 = new User(c.getString(0),c.getString(1),c.getString(2),c.getString(3));

        // utiliser la méthode native : findViewById()
        // pour récupérer les infos du bouton Valider
        bt_valider = (Button) findViewById(R.id.btValider);
        // permet d'écouter les informations du bouton
        bt_valider.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                et_password =(EditText) findViewById(R.id.editPassword);
                Log.v("Main1", et_password.getText().toString());
                Log.v("Main2",Param.MDP_USER.toString());
                Log.v("Main3",user1.getMotDPasse().toString());

                if(Param.MDP_USER.equals(et_password.getText().toString())){
                    //initialise une pop up longue
                    Toast toast = Toast.makeText(MainActivity.this, "Veuillez modifier votre mot de passe", Toast.LENGTH_LONG);
                    //affiche la pop up
                    toast.show();
                    //initialise un transfert d'activity
                    Intent myIntent = new Intent(MainActivity.this, ParamActivity.class);
                    //lance le transfert
                    startActivity(myIntent);
                } else if (et_password.getText().toString().equals(user1.getMotDPasse().toString())){
                    //initialise une pop up longue
                    Toast toast = Toast.makeText(MainActivity.this, "Bonjour Mr "+ user1.getNom(), Toast.LENGTH_SHORT);
                    //affiche la pop up
                    toast.show();
                    //initialise un transfert d'activity
                    Intent myIntent = new Intent(MainActivity.this, TabbordActivity.class);
                    //lance le transfert
                    startActivity(myIntent);
                } else {
                    //initialise une pop up longue
                    Toast toast = Toast.makeText(MainActivity.this, "Votre mot de passe est incorrect", Toast.LENGTH_LONG);
                    //affiche la pop up
                    toast.show();
                }

            }
        });
    }
}
