package fr.severineguimier.universalpharma.Activite;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import fr.severineguimier.universalpharma.Dao.Dao;
import fr.severineguimier.universalpharma.Dao.Param;
import fr.severineguimier.universalpharma.Metier.User;
import fr.severineguimier.universalpharma.R;

public class ParamActivity extends AppCompatActivity {
    private TextView et_matricule;
    private TextView et_nom;
    private TextView et_prenom;
    private EditText et_mdp;
    private Button bt_envoyer;

    private Dao dao;
    private Cursor c;
    private User user1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_param);

        et_matricule =(TextView) findViewById(R.id.editMatricule);
        et_nom =(TextView) findViewById(R.id.editNom);
        et_prenom =(TextView) findViewById(R.id.editPrenom);

        dao = new Dao(this);
        c = dao.selectOneUser(Param.MATRICULE_USER);
        c.moveToFirst();
        user1 = new User(c.getString(0),c.getString(1),c.getString(2),c.getString(3));

        et_matricule.setText(user1.getId());
        et_nom.setText(user1.getNom());
        et_prenom.setText(user1.getPrenom());

        bt_envoyer = (Button) findViewById(R.id.btEnvoyer);
        // permet d'écouter les informations du bouton
        bt_envoyer.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                et_mdp =(EditText) findViewById(R.id.editMdp);
                /*Log.v("Param", et_mdp.getText().toString());*/
                if(et_mdp.getText().toString().equals("")){
                    Toast toast = Toast.makeText(ParamActivity.this, "Veuillez renseigner le mot de passe", Toast.LENGTH_LONG);
                    toast.show();
                } else {
                    user1.setMotDPasse(et_mdp.getText().toString());
                    dao.updateUser(user1);
                    Toast toast = Toast.makeText(ParamActivity.this, "Votre mot de passe a été modifié", Toast.LENGTH_LONG);
                    toast.show();
                    Intent myIntent = new Intent(ParamActivity.this, TabbordActivity.class);
                    startActivity(myIntent);
                }

            }
        });
    }
}
