package fr.severineguimier.universalpharma.Activite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import fr.severineguimier.universalpharma.Dao.Dao;
import fr.severineguimier.universalpharma.R;

public class SyntheseActivity extends AppCompatActivity {
    private TextView txFaf;
    private TextView txFhf;
    private TextView txTotal;

    private Dao dao;

    private Button btretour;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_synthese);

        dao = new Dao(this);
        txFaf = (TextView) findViewById(R.id.texteuroFaf);
        txFhf = (TextView) findViewById(R.id.texteuroFhf);
        txTotal = (TextView) findViewById(R.id.texteuroTotal);

        //calcul des frais au forfait
        String faf = dao.sommefaf();
        Log.v("Synth1", faf);
        Float faff = Float.parseFloat(faf);
        faf = faf + " Euros";
        txFaf.setText(faf);

        //calcul des frais hors forfait
        String fhf = dao.sommefhf();
        Log.v("Synth2", fhf);
        Float fhff = Float.parseFloat(fhf);
        fhf = fhf + " Euros";
        txFhf.setText(fhf);

        //calcul des frais totaux
        Float resu = faff + fhff;
        String resus = resu.toString();
        txTotal.setText(resus + " Euros");

        btretour = (Button) findViewById(R.id.btRetour);
        // permet d'écouter les informations du bouton
        btretour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //initialise un transfert d'activity
                Intent myIntent = new Intent(SyntheseActivity.this, TabbordActivity.class);
                //lance le transfert
                startActivity(myIntent);
            }
        });

    }
}
