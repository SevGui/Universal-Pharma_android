package fr.severineguimier.universalpharma.Activite;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import fr.severineguimier.universalpharma.R;

public class TabbordActivity extends AppCompatActivity {
    private Button bt_faf;
    private Button bt_fhf;
    private Button bt_param;
    private Button bt_maj;
    private Button bt_synth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbord);

        bt_faf = (Button) findViewById(R.id.btFAF);
        bt_faf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(TabbordActivity.this, FafActivity.class);
                startActivity(myIntent);
            }
        });

        bt_fhf = (Button) findViewById(R.id.btFHF);
        bt_fhf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(TabbordActivity.this, FhfActivity.class);
                startActivity(myIntent);
            }
        });

        bt_param = (Button) findViewById(R.id.btParam);
        bt_param.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(TabbordActivity.this, ParamActivity.class);
                startActivity(myIntent);
            }
        });

        bt_maj = (Button) findViewById(R.id.btMiseaJour);
        // permet d'écouter les informations du bouton
        bt_maj.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //initialise un transfert d'activity
                Intent myIntent = new Intent(TabbordActivity.this, TransfertActivity.class);
                //lance le transfert
                startActivity(myIntent);
            }
        });

        bt_synth = (Button) findViewById(R.id.btSynthese);
        // permet d'écouter les informations du bouton
        bt_synth.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //initialise un transfert d'activity
                Intent myIntent = new Intent(TabbordActivity.this, SyntheseActivity.class);
                //lance le transfert
                startActivity(myIntent);
            }
        });
    }
}
