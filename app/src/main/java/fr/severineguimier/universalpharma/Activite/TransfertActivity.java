package fr.severineguimier.universalpharma.Activite;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import au.com.bytecode.opencsv.CSVWriter;
import fr.severineguimier.universalpharma.Dao.Dao;
import fr.severineguimier.universalpharma.Dao.Param;
import fr.severineguimier.universalpharma.Metier.User;
import fr.severineguimier.universalpharma.R;

public class TransfertActivity extends AppCompatActivity {
    private Dao dao;
    private Cursor c1, c2;

    private File fichierAEnvoyer1, fichierAEnvoyer2;
    private RequestBody requestBody;
    private Request request;
    private Response response;

    private final OkHttpClient client = new OkHttpClient();

    private static Button bt_transfert;
    private static RadioButton radioButtonCSV;
    private static RadioButton radioButtonServeur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfert);

        dao = new Dao(this);
        radioButtonCSV = (RadioButton) findViewById(R.id.radioButtonCSV);
        radioButtonServeur = (RadioButton) findViewById(R.id.radioButtonServeur);

        int n = construireFichierCsv();

        radioButtonCSV.setChecked(true);

        choixFichier();

        radioButtonServeur.setChecked(true);

        bt_transfert = (Button) findViewById(R.id.btTransfert);
        // permet d'écouter les informations du bouton
        bt_transfert.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                Uri url = Uri.parse(Param.SITE_WEB +"?time="+(new Date().getTime()));
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(url);
                startActivity(i);

                try {
                    Thread.sleep(6000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //Supprime tous les frais
                dao.deleteFafs();
                dao.deleteFhfs();
                radioButtonCSV.setChecked(false);
                radioButtonServeur.setChecked(false);

                //on renvoit vers mainactivity
                Intent myIntent = new Intent(TransfertActivity.this, TabbordActivity.class);
                //lance le transfert
                startActivity(myIntent);
            }
        });
    }

//-------------------------------------------------------------------------------------------------
    private int construireFichierCsv() {
        // met le chemin du dossier dans la variable chemin
        //storage/emulated/0
        String chemin = Environment.getExternalStorageDirectory().getPath();
        int nbre = 0;
        //chemin du fichier plus le nom du fichier
        File file1 = new File(chemin+"/"+ "listeFaf.csv");
        File file2 = new File(chemin+"/"+ "listeFhf.csv");

 /*       //initialise une pop up longue
        Toast toast = Toast.makeText(TransfertActivity.this, file1.toString(), Toast.LENGTH_LONG);
        //affiche la pop up
        toast.show();*/

        try {
            //crée le fichier csv
            file1.createNewFile();
            file2.createNewFile();

            //le mets en écriture
            CSVWriter csvWrite1 = new CSVWriter(new FileWriter(file1));
            CSVWriter csvWrite2 = new CSVWriter(new FileWriter(file2));

            //Sélectionne les frais au forfait
            c1 = dao.selectFaf();
            c2 = dao.selectFhf();
            //Log.v("Envoi2", c.toString());

            //récupération de la date d'aujourd'hui au format yyyyMM
            String date = new SimpleDateFormat("yyyyMM").format(new Date());

            while (c1.moveToNext()){
                //sélectionne les infos des 5 colonnes
                String arrStr[]={Param.MATRICULE_USER, date, c1.getString(1), c1.getString(2)};
                //Log.v("Envoi1", arrStr.toString());

                //écrit dans le fichier csv
                csvWrite1.writeNext(arrStr);
                nbre++;
            }

              nbre = 0;
                while (c2.moveToNext()){
                //sélectionne les infos des 5 colonnes
                String arrStr[]={Param.MATRICULE_USER, date, c2.getString(1), c2.getString(3),c2.getString(2),c2.getString(4)};
                //Log.v("Envoi1", arrStr.toString());

                //écrit dans le fichier csv
                csvWrite2.writeNext(arrStr);
                nbre++;
            }

            //fermer l'écriture
            csvWrite1.close();
            c1.close();
            csvWrite2.close();
            c2.close();

        } catch (Exception sqlEx) {
            Log.e("MainActivity", sqlEx.getMessage(), sqlEx);
        }
        return nbre;
    }

    //----------------------------------------------------------------------------------------------

    protected void choixFichier() {
        //sélectionne le chemin
        String chemin = Environment.getExternalStorageDirectory().getPath();
        //donne le chemin et le nom du fichier
        fichierAEnvoyer1 = new File(chemin + "/" + "listeFaf.csv");
        fichierAEnvoyer2 = new File(chemin + "/" + "listeFhf.csv");

        try {
/*            //crée un toast pour prévenir
            Toast.makeText(TransfertActivity.this, "Document prêt à l'envoi", Toast.LENGTH_LONG).show();*/
            //appelle de la fonction
            envoiVersServeur(fichierAEnvoyer1);
            envoiVersServeur(fichierAEnvoyer2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //----------------------------------------------------------------------------------------------

    private void envoiVersServeur(final File file) {
        final AsyncTask<Void, Integer, String> execute = new AsyncTask<Void, Integer, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... params) {

                //le corps (requestbody): pour le contenu de votre requête
                //crée le corps de la requete http
                requestBody = new MultipartBuilder().type(MultipartBuilder.FORM)
                        .addFormDataPart(Param.NOM_FICHIER, file.getName(), RequestBody
                                .create(MediaType.parse("text/txt"),file)).build();

                //lien request avec l'url
                //crée la requete http avec le corps précédent
                request = new Request.Builder().url(Param.URL).post(requestBody).build();
                response = null;

                try {
                    //on execute la requête http
                    //client okhttpclient pour la communication
                    response = client.newCall(request).execute();
                    String responseStr = response.body().string();
                    return responseStr;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }
}
