package fr.severineguimier.universalpharma.Dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import fr.severineguimier.universalpharma.Activite.MainActivity;
import fr.severineguimier.universalpharma.Metier.Faf;
import fr.severineguimier.universalpharma.Metier.Fhf;
import fr.severineguimier.universalpharma.Metier.User;

/**
 * Created by Severine on 31/08/2016.
 */
public class Dao extends SQLiteOpenHelper implements InterfaceDao {
    private SQLiteDatabase db;
    private User user1;

    public Dao(Context context) {
        super(context, Param.DATABASE_NAME, null, Param.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sqlFaf = " CREATE TABLE " + Param.TABLE_FAF + "(" +
                Param.COL_ID_FAF + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Param.COL_TF_FAF + " TEXT," +
                Param.COL_QUANTITE_FAF + " INTEGER," +
                "FOREIGN KEY(" + Param.COL_TF_FAF + ") REFERENCES " + Param.TABLE_TF + "(" + Param.COL_ID_TF + "))";
        db.execSQL(sqlFaf);

        String sqlFhf = " CREATE TABLE " + Param.TABLE_FHF + "(" +
                Param.COL_ID_FHF + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Param.COL_LIBELLE_FHF + " TEXT," +
                Param.COL_MONTANT_FHF + " REAL," +
                Param.COL_DATEJ_FHF + " TEXT," +
                Param.COL_NOMJUSTIF_FHF + " TEXT)";
        db.execSQL(sqlFhf);

        String sqlUser = " CREATE TABLE " + Param.TABLE_USER + "(" +
                Param.COL_ID_USER + " TEXT PRIMARY KEY, " +
                Param.COL_NOM_USER + " TEXT," +
                Param.COL_PRENOM_USER + " TEXT," +
                Param.COL_MDP_USER + " TEXT)";
        db.execSQL(sqlUser);

        String sqlTypeFrais = " CREATE TABLE " + Param.TABLE_TF + "(" +
                Param.COL_ID_TF + " TEXT PRIMARY KEY, " +
                Param.COL_LIBELLE_TF + " TEXT," +
                Param.COL_MONTANT_TF + " REAL)";
        db.execSQL(sqlTypeFrais);

        addTF(db);
        user1 = new User(Param.MATRICULE_USER, Param.NOM_USER, Param.PRENOM_USER, Param.MDP_USER);
        createUser(user1, db);
    }

    private void addTF(SQLiteDatabase db) {
        ContentValues tabTypeFrais = new ContentValues();
        tabTypeFrais.put(Param.COL_ID_TF, "ETP");
        tabTypeFrais.put(Param.COL_LIBELLE_TF, "Forfait Etape");
        tabTypeFrais.put(Param.COL_MONTANT_TF, 110);
        db.insert(Param.TABLE_TF, null, tabTypeFrais);
        tabTypeFrais.put(Param.COL_ID_TF, "NUI");
        tabTypeFrais.put(Param.COL_LIBELLE_TF, "Nuitée Hôtel");
        tabTypeFrais.put(Param.COL_MONTANT_TF, 80);
        db.insert(Param.TABLE_TF, null, tabTypeFrais);
        tabTypeFrais.put(Param.COL_ID_TF, "REP");
        tabTypeFrais.put(Param.COL_LIBELLE_TF, "Repas Restaurant");
        tabTypeFrais.put(Param.COL_MONTANT_TF, 25);
        db.insert(Param.TABLE_TF, null, tabTypeFrais);
        tabTypeFrais.put(Param.COL_ID_TF, "4cvd");
        tabTypeFrais.put(Param.COL_LIBELLE_TF, "4 chevaux Diesel");
        tabTypeFrais.put(Param.COL_MONTANT_TF, 0.52);
        db.insert(Param.TABLE_TF, null, tabTypeFrais);
        tabTypeFrais.put(Param.COL_ID_TF, "4cve");
        tabTypeFrais.put(Param.COL_LIBELLE_TF, "4 chevaux Essence");
        tabTypeFrais.put(Param.COL_MONTANT_TF, 0.62);
        db.insert(Param.TABLE_TF, null, tabTypeFrais);
        tabTypeFrais.put(Param.COL_ID_TF, "56cvd");
        tabTypeFrais.put(Param.COL_LIBELLE_TF, "5/6 chevaux Diesel");
        tabTypeFrais.put(Param.COL_MONTANT_TF, 0.58);
        db.insert(Param.TABLE_TF, null, tabTypeFrais);
        tabTypeFrais.put(Param.COL_ID_TF, "56cve");
        tabTypeFrais.put(Param.COL_LIBELLE_TF, "5/6 chevaux Essence");
        tabTypeFrais.put(Param.COL_MONTANT_TF, 0.67);
        db.insert(Param.TABLE_TF, null, tabTypeFrais);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + Param.TABLE_FAF);
        db.execSQL("DROP TABLE " + Param.TABLE_FHF);
        db.execSQL("DROP TABLE " + Param.TABLE_TF);
        db.execSQL("DROP TABLE " + Param.TABLE_USER);
        this.onCreate(db);
    }

    @Override
    public void open() {
        db = this.getWritableDatabase();
    }

    @Override
    public synchronized void close() {
        this.close();
    }

    //-----------------------------------------------------------------
    // Partie Utilisateurs

    @Override
    public long createUser(User user, SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(Param.COL_ID_USER, user.getId());
        values.put(Param.COL_NOM_USER, user.getNom());
        values.put(Param.COL_PRENOM_USER, user.getPrenom());
        values.put(Param.COL_MDP_USER, user.getMotDPasse());
/*        Log.v("Dao", values.toString());*/
        long insertID = db.insert(Param.TABLE_USER, null, values);
        return insertID;
    }

    @Override
    public void deleteUser(User user) {
        open();
        String id = user.getId();
        db.delete(Param.TABLE_USER, Param.COL_ID_USER + " = " + id, null);
    }

    @Override
    public void deleteUsers() {
        open();
        db.delete(Param.TABLE_USER, null, null);
    }

    @Override
    public int updateUser(User user) {
        open();
        ContentValues values = new ContentValues();
        String id = user.getId();
        values.put(Param.COL_NOM_USER, user.getNom());
        values.put(Param.COL_PRENOM_USER, user.getPrenom());
        values.put(Param.COL_MDP_USER, user.getMotDPasse());
        Log.v("Dao2", values.toString());
        Log.v("Dao3", id);
        int updateID = db.update(Param.TABLE_USER, values, Param.COL_ID_USER + " = ?", new String[]{id});
        /*int updateID = 0;*/
        return updateID;
    }

    @Override
    public Cursor selectUser() {
        SQLiteDatabase db = this.getReadableDatabase();
        String req = "SELECT * FROM " + Param.TABLE_USER;
        Cursor cur = db.rawQuery(req, null);
        return cur;
    }

    public Cursor selectOneUser(String id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String req = "SELECT * FROM " + Param.TABLE_USER + " WHERE " + Param.COL_ID_USER + " = ?";
  /*      Log.v("Dao1", req);*/
        /*String data[] = new String[]{id};
        Log.v("Dao2", data[0].toString());*/
        Cursor cur = db.rawQuery(req, new String[]{id});
        return cur;
    }

    @Override
    public int countUser() {
        int count = 0;
        open();
        String req = "SELECT COUNT(*) FROM " + Param.TABLE_USER;
        Cursor c = db.rawQuery(req, null);
        c.moveToFirst();
        count = c.getInt(0);
        c.close();
        return count;
    }

    //------------------------------------------------------------------------------------------
    //partie Frais au forfait

    @Override
    public long createFaf(Faf faf) {
        open();
        ContentValues values = new ContentValues();
        values.put(Param.COL_TF_FAF, faf.getCategorieTF());
        values.put(Param.COL_QUANTITE_FAF, faf.getQuantite());
        long insertID = db.insert(Param.TABLE_FAF, null, values);
        return insertID;
    }

    @Override
    public void deleteFaf(Faf faf) {
        open();
        Long id = faf.getId();
        db.delete(Param.TABLE_FAF, Param.COL_ID_FAF + " = " + id, null);
    }

    @Override
    public void deleteFafs() {
        open();
        db.delete(Param.TABLE_FAF, null, null);
    }

    @Override
    public int updateFaf(Faf faf) {
        open();
        ContentValues values = new ContentValues();
        Long id = faf.getId();
        values.put(Param.COL_TF_FAF, faf.getCategorieTF());
        values.put(Param.COL_QUANTITE_FAF, faf.getQuantite());
        int updateID = db.update(Param.TABLE_FAF, values, Param.COL_ID_FAF + " = " + id, null);
        return updateID;
    }

    @Override
    public Cursor selectFaf() {
        SQLiteDatabase db = this.getReadableDatabase();
        String req = "SELECT * FROM " + Param.TABLE_FAF;
        Cursor cur = db.rawQuery(req, null);
        return cur;
    }

    public String sommefaf() throws SQLException {
        String resu = "";
        SQLiteDatabase db = this.getReadableDatabase();
        String req = "SELECT sum(" + Param.COL_QUANTITE_FAF + "*" + Param.COL_MONTANT_TF +
                ") FROM " + Param.TABLE_FAF + " AS FAF JOIN " + Param.TABLE_TF +
                " AS TF ON FAF." + Param.COL_TF_FAF + " = TF." + Param.COL_ID_TF;
        Log.v("Dao3", req);
        Cursor cur = db.rawQuery(req, null);
        Log.v("Dao3-1", cur.toString());
        Log.v("Dao3-2", Boolean.toString(cur.moveToFirst()));
            cur.moveToFirst();
        Log.v("Dao4", cur.getColumnName(0));
        Log.v("Dao4-1", cur.getString(0));

        resu = cur.getString(0);
        return resu;
    }

    //------------------------------------------------------------------------------------------
    //partie Frais hors forfait

    @Override
    public long createFhf(Fhf fhf) {
        open();
        ContentValues values = new ContentValues();
        values.put(Param.COL_LIBELLE_FHF, fhf.getLibelleFHF());
        values.put(Param.COL_MONTANT_TF, fhf.getMontantFHF());
        values.put(Param.COL_DATEJ_FHF, fhf.getDateJFHF());
        values.put(Param.COL_NOMJUSTIF_FHF, fhf.getNomJustifFHF());
        long insertID = db.insert(Param.TABLE_FHF, null, values);
        return insertID;
    }

    @Override
    public void deleteFhf(Fhf fhf) {
        open();
        Long id = fhf.getId();
        db.delete(Param.TABLE_FHF, Param.COL_ID_FHF + " = " + id, null);
    }

    @Override
    public void deleteFhfs() {
        open();
        db.delete(Param.TABLE_FHF, null, null);
    }

    @Override
    public int updateFhf(Fhf fhf) {
        open();
        ContentValues values = new ContentValues();
        Long id = fhf.getId();
        values.put(Param.COL_LIBELLE_FHF, fhf.getLibelleFHF());
        values.put(Param.COL_MONTANT_TF, fhf.getMontantFHF());
        values.put(Param.COL_DATEJ_FHF, fhf.getDateJFHF());
        values.put(Param.COL_NOMJUSTIF_FHF, fhf.getNomJustifFHF());
        int updateID = db.update(Param.TABLE_FHF, values, Param.COL_ID_FHF + " = " + id, null);
        return updateID;
    }

    @Override
    public Cursor selectFhf() {
        SQLiteDatabase db = this.getReadableDatabase();
        String req = "SELECT * FROM " + Param.TABLE_FHF;
        Cursor cur = db.rawQuery(req, null);
        return cur;
    }

    public String sommefhf() {
        String resu = "";
        SQLiteDatabase db = this.getReadableDatabase();
        String req = "SELECT sum(" + Param.COL_MONTANT_FHF + ") FROM " + Param.TABLE_FHF;
        Log.v("Dao5", req);
        Cursor cur = db.rawQuery(req, null);
        cur.moveToFirst();
        Log.v("Dao6", cur.getString(0));
        resu = cur.getString(0);
        return resu;
    }

}
