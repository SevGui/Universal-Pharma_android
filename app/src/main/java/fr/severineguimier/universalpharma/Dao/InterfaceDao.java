package fr.severineguimier.universalpharma.Dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import fr.severineguimier.universalpharma.Metier.Faf;
import fr.severineguimier.universalpharma.Metier.Fhf;
import fr.severineguimier.universalpharma.Metier.User;


/**
 * Created by Severine on 30/08/2016.
 */
public interface InterfaceDao {

    public abstract void open();

    public void close();

    public long createUser(User user, SQLiteDatabase db);

    public void deleteUser(User user);

    public void deleteUsers();

    public int updateUser(User user);

    public Cursor selectUser();

    public int countUser();

    //----------------------------

    public long createFaf(Faf faf);

    public void deleteFaf(Faf faf);

    public void deleteFafs();

    public int updateFaf(Faf faf);

    public Cursor selectFaf();

    //-----------------------------
    public long createFhf(Fhf fhf);

    public void deleteFhf(Fhf fhf);

    public void deleteFhfs();

    public int updateFhf(Fhf fhf);

    public Cursor selectFhf();
}
