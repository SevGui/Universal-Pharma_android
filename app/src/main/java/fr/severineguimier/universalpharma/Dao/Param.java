package fr.severineguimier.universalpharma.Dao;

/**
 * Created by Severine on 30/08/2016.
 */
public class Param {
    public static final int DATABASE_VERSION = 1;

    //Nom de la base de donnée
    public static final String DATABASE_NAME = "uniphama";

    //Nom de la table et des colonnes user
    public static final String TABLE_USER = "user";
    public static final String COL_ID_USER = "_id";
    public static final String COL_NOM_USER = "nom";
    public static final String COL_PRENOM_USER = "prenom";
    public static final String COL_MDP_USER = "mdp";

    //Utilisateur du téléphone
    public static final String MATRICULE_USER = "a93";
    public static final String NOM_USER = "Tusseau";
    public static final String PRENOM_USER = "Louis";
    public static final String MDP_USER = "123456";


    //Nom de la table et des colonnes faf
    public static final String TABLE_FAF = "faf";
    public static final String COL_ID_FAF = "_id";
    public static final String COL_TF_FAF = "typefrais";
    public static final String COL_QUANTITE_FAF = "quantite";

    //Nom de la table et des colonnes fhf
    public static final String TABLE_FHF = "fhf";
    public static final String COL_ID_FHF = "_id";
    public static final String COL_LIBELLE_FHF = "libelle";
    public static final String COL_MONTANT_FHF = "montantTF";
    public static final String COL_DATEJ_FHF = "datej";
    public static final String COL_NOMJUSTIF_FHF = "nomjustif";
    public static final int IMAGE_GALLERY_REQUEST = 50;

    //Nom de la table et des colonnes type frais
    public static final String TABLE_TF = "typefrais";
    public static final String COL_ID_TF = "_id";
    public static final String COL_LIBELLE_TF = "libelleTF";
    public static final String COL_MONTANT_TF = "montantTF";

    //Constante pour le transfert vers la base UP
    public static final String NOM_FICHIER = "file";
    //url pointant sur le fichier qui envoie un fichier vers le serveur
    public static final String URL = "http://10.0.2.2/upload.php";
    //load_csv.php contient le code php permettant d'insérer le
    // fichier csv dans la table correspondante
    public static final String SITE_WEB ="http://10.0.2.2/DossierUP/load_csv.php";
}
