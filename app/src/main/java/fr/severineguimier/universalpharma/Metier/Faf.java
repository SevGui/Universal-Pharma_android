package fr.severineguimier.universalpharma.Metier;

/**
 * Created by Severine on 31/08/2016.
 */
public class Faf {
    private Long id;
    private String categorieTF;
    private int quantite;

    public Faf() {
    }

    public Faf( String categorieTF, int quantite) {
        this.quantite = quantite;
        this.categorieTF = categorieTF;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public String getCategorieTF() {
        return categorieTF;
    }

    public void setCategorieTF(String categorieTF) {
        this.categorieTF = categorieTF;
    }
}
