package fr.severineguimier.universalpharma.Metier;

import java.util.Date;

/**
 * Created by Severine on 02/09/2016.
 */
public class Fhf {
    private Long id;
    private String libelleFHF;
    private Float montantFHF;
    private String dateJFHF;
    private String nomJustifFHF;

    public Fhf() {
    }

    public Fhf(String libelleFHF, Float montantFHF, String dateJFHF, String nomJustifFHF) {
        this.libelleFHF = libelleFHF;
        this.montantFHF = montantFHF;
        this.dateJFHF = dateJFHF;
        this.nomJustifFHF = nomJustifFHF;
    }

    public Fhf(String libelleFHF, Float montantFHF) {
        this.libelleFHF = libelleFHF;
        this.montantFHF = montantFHF;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelleFHF() {
        return libelleFHF;
    }

    public void setLibelleFHF(String libelleFHF) {
        this.libelleFHF = libelleFHF;
    }

    public Float getMontantFHF() {
        return montantFHF;
    }

    public void setMontantFHF(Float montantFHF) {
        this.montantFHF = montantFHF;
    }

    public String getDateJFHF() {
        return dateJFHF;
    }

    public void setDateJFHF(String dateJFHF) {
        this.dateJFHF = dateJFHF;
    }

    public String getNomJustifFHF() {
        return nomJustifFHF;
    }

    public void setNomJustifFHF(String nomJustifFHF) {
        this.nomJustifFHF = nomJustifFHF;
    }
}
