package fr.severineguimier.universalpharma.Metier;

/**
 * Created by Severine on 30/08/2016.
 */
public class User {
    private String id, nom, prenom, motDPasse;

    public User() {
    }

    public User(String id, String nom, String prenom, String motDPasse) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.motDPasse = motDPasse;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMotDPasse() {
        return motDPasse;
    }

    public void setMotDPasse(String motDPasse) {
        this.motDPasse = motDPasse;
    }

    public String getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


}
